const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
   cors: { origin: "*"}
});
const axios = require('axios'); // Import Axios

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
io.on('connection', (socket) => {
  // console.log('Client connected');
//   socket.on('send-message', (message) => {
//     console.log(message);
    io.emit('log-message', 'Client connected');
//   });

 socket.on('recieve-answer', (message) => {
    var dataToSend = JSON.parse(message);
    console.log(dataToSend);

    // Use an HTTP client to send the data to your Laravel API
    const bearerToken = dataToSend.token;

    let apiEndpoint;
    if (dataToSend.room_type === 'human') {
        apiEndpoint = 'https://smartypants.pro/api/recieve-answer-socket';
    } else if (dataToSend.room_type === 'bot') {
        apiEndpoint = 'https://smartypants.pro/api/recieve-answer-socket-for-bot';
    } else {
        // Handle the case where room_type is neither "human" nor "bot"
        io.emit('log-message', 'Invalid room_type received');
        return;
    }

    axios.post(apiEndpoint, {
        room_id: dataToSend.room_id,
        question_id: dataToSend.question_id,
        user_answer: dataToSend.user_answer,
        user_id: dataToSend.user_id,
        game_status: dataToSend.game_status,
        room_type: dataToSend.room_type,
        game_type: dataToSend.game_type
    }, {
        headers: {
            'Authorization': `Bearer ${bearerToken}`
        }
    })
    .then(response => {
        // Handle the response
        console.log(response.data);
        io.emit('answer-response', response.data);
        io.emit('log-message', response.data);
    })
    .catch(error => {
        io.emit('answer-response', error);
        io.emit('log-message', error);
        console.error(error);
    });
});

  // for play agian
  socket.on('play-again-request-response', (message) => {
    var dataToSend = JSON.parse(message)
    console.log(dataToSend);
    // return;
    // Use an HTTP client to send the data to your Laravel API
    const bearerToken = dataToSend.token;

    axios.post('https://smartypants.pro/api/recieve-play-again-response-socket', {
      request_id: dataToSend.request_id,
      }, {
        headers: {
          'Authorization': `Bearer ${bearerToken}`
        }
      })
      .then(response => {
        // Handle the response
        console.log(response.data);
        io.emit('play-again-request-response', response.data);
        io.emit('log-message', response.data);
      })
      .catch(error => {
        io.emit('play-again-request-response', error);
        io.emit('log-message', error);
        console.error(error);
      });
  });
  //end play again 
  socket.on('disconnect', () => {
    console.log('Client disconnected');
    io.emit('log-message', 'Client disconnected');
  });
});

app.use(express.static('index.html'));
app.use(express.json());
app.use(express.urlencoded());
app.post('/send-data', (req, res) => {
    const user = req.body; // Access the user data
    if (user) {
        // const user = JSON.parse(userData); // Parse the JSON data
        console.log('Data received from Laravel:', user);

        // Process the user data and potentially emit it to Socket.io clients
        io.emit('new-message', user);
        res.json({ message: 'Data received and sent to Socket.io clients' });
        io.emit('log-message', user);
    } else {
        res.status(400).json({ message: 'Invalid user data received' });
        console.log('Invalid user data received');
        io.emit('log-message', 'Invalid user data received');
    }
});
app.post('/send-notification', (req, res) => {
    const user = req.body; // Access the user data
    if (user) {
        // const user = JSON.parse(userData); // Parse the JSON data
        console.log('Data received from Laravel:', user);

        // Process the user data and potentially emit it to Socket.io clients
        io.emit('play-again-request', user);
        res.json({ message: 'Data received and sent to Socket.io clients' });
        io.emit('log-message', user);
    } else {
        res.status(400).json({ message: 'Invalid user data received' });
        console.log('Invalid user data received');
        io.emit('log-message', 'Invalid user data received');
    }
});
app.post('/send-game-request-notification', (req, res) => {
  const user = req.body; // Access the user data
  if (user) {
      // const user = JSON.parse(userData); // Parse the JSON data
      console.log('Data received from Laravel:', user);

      // Process the user data and potentially emit it to Socket.io clients
      io.emit('game-request', user);
      res.json({ message: 'Data received and sent to Socket.io clients' });
      io.emit('log-message', user);
  } else {
      res.status(400).json({ message: 'Invalid user data received' });
      console.log('Invalid user data received');
      io.emit('log-message', 'Invalid user data received');
  }
});
const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
